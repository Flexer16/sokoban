---
-- entity_factory.lua


local class = require "middleclass"

local EntityTypes = require "utils.enums.entity_types"


local EntityFactory = class("EntityFactory")

function EntityFactory:initialize(data)
    self.position, self.tile, self.type, self.map, self.creature, self.player, self.box_data = Component.load({
        "Position",
        "Tile",
        "Type",
        "Map",
        "Creature",
        "Player",
        "BoxData"
    })
end

function EntityFactory:get_map_entity(data)
    local entity = Entity()
    entity:initialize()
    entity:add(self.map())
    entity:add(self.position({x = data.x, y = data.y}))
    entity:add(self.tile({tile = data.tile}))

    local cur_type = EntityTypes.floor

    if data.tile == "#" then
        cur_type = EntityTypes.wall
    end

    if data.tile == "X" then
        cur_type = EntityTypes.box_point
        entity:add(self.box_data({box_point = true}))
    end

    entity:add(self.type({type = cur_type}))

    return entity
end

function EntityFactory:get_creature_entity(data)
    local entity = Entity()
    entity:initialize()
    entity:add(self.creature())
    entity:add(self.position({x = data.x, y = data.y}))
    entity:add(self.tile({tile = data.tile}))

    if data.tile == "@" then
        entity:add(self.player())
    end

    if data.tile == "b" then
        entity:add(self.type({type = EntityTypes.box}))
        entity:add(self.box_data({box = true}))
    end

    return entity
end

return EntityFactory
