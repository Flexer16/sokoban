---
-- event_factory.lua


local class = require "middleclass"

local MovingActions = require "utils.enums.moving_actions"


local EventFactory = class("EventFactory")

function EventFactory:initialize(data)
    self.load_scene_event, self.beautifire_map_event, self.player_action_event, self.moving_event, self.clear_event, self.ui_data_event = Component.load({
        "LoadSceneEvent",
        "BeautifireMapEvent",
        "PlayerActionEvent",
        "MovingEvent",
        "ClearEvent",
        "UiDataEvent"
    })
end

function EventFactory:rise_load_scene(file_path)
    local load_scene = Entity()
    load_scene:initialize()
    load_scene:add(self.load_scene_event({scene_data_path = file_path}))

    engine:addEntity(load_scene)
end

function EventFactory:rise_map_beautifire(wall_list)
    local beautifire_map = Entity()
    beautifire_map:initialize()
    beautifire_map:add(self.beautifire_map_event({wall_list = wall_list}))

    engine:addEntity(beautifire_map)
end

function EventFactory:rise_player_action(action_name)
    local moving_action = Entity()
    moving_action:initialize()
    moving_action:add(self.moving_event({action = MovingActions.player_move, extra = action_name, player = true}))

    engine:addEntity(moving_action)
end

function EventFactory:rise_add_physics_object(object_data, is_player)
    if not is_player then
        is_player = false
    end

    local moving_action = Entity()
    moving_action:initialize()
    moving_action:add(self.moving_event({action = MovingActions.add_entity, extra = object_data, player = is_player}))

    engine:addEntity(moving_action)
end

function EventFactory:rise_remove_all_objects()
    local remove_all_action = Entity()
    remove_all_action:initialize()
    remove_all_action:add(self.moving_event({action = MovingActions.remove_all, extra = nil, player = nil}))

    engine:addEntity(remove_all_action)
end

function EventFactory:rise_clear_all()
    local clear_action = Entity()
    clear_action:initialize()
    clear_action:add(self.clear_event())

    engine:addEntity(clear_action)
end

function EventFactory:rise_ui_data(event_type, extra)
    local ui_data = Entity()
    ui_data:initialize()
    ui_data:add(self.ui_data_event({event_type = event_type, extra = extra}))

    engine:addEntity(ui_data)
end

return EventFactory
