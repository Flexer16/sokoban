---
-- matrix.lua
--
-- Прямоугольная матрица


local class = require "middleclass"


local Matrix = class("Matrix")

---
-- Конструктор. Создание марицы заданого размера
function Matrix:initialize(x, y, default)
    local value = default or nil

    self.width, self.height = x, y
    self.map = {}

    for j = 1, self.height do
        for i = 1, self.width do
            self.map[(i - 1) * self.width + j] = value
        end
    end
end

---
-- Получить ширину матрицы
function Matrix:get_widht()
    return self.width
end

---
-- Получить высоту матрицы
function Matrix:get_height()
    return self.height
end

---
-- Получить объект, находящийся по заданым координатам
function Matrix:get(x, y)
    return self.map[(x - 1) * self.width + y]
end

---
-- Установить объект в заданые координаты
function Matrix:set(x, y, value)
    self.map[(x - 1) * self.width + y] = value
end

---
-- Итератор по всем клеткам в матрице
function Matrix:iterate()
    local i, j = 0, 1

    return function ()
        if i < self.width then
            i = i + 1
        else
            if j < self.height  then
                j = j + 1
                i = 1
            else
                return nil
            end
        end

        return i, j, self.map[(i - 1) * self.width + j]
    end
end

return Matrix
