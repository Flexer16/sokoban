---
-- log.lua


local class = require "middleclass"


local Log = class("Class")

Log.static.lovebird = require "lovebird"

function Log:initialize(data)
    self.component = "None"

    if data then
        if data.component then
            self.component = data.component
        end
    end
end

function Log:update()
    Log.lovebird.update()
end

function Log:log(msg)
    Log.lovebird.print("[" .. self.component .. "] " .. msg)
end

return Log
