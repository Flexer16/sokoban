---
-- moving_actions.lua


local MovingActions = {
    add_entity = "add_entity",
    player_move = "player_move",
    remove_all = "remove_all"
}

return MovingActions
