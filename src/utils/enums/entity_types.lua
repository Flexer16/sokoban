---
-- entity_types.lua


local EntityTypes = {
    wall = "wall",
    floor = "floor",
    box_point = "box_point",
    box = "box"
}

return EntityTypes
