---
-- moving_directions.lua


local MovingDirections = {
    move_up = "move_up",
    move_down = "move_down",
    move_left = "move_left",
    move_right = "move_right"
}

return MovingDirections
