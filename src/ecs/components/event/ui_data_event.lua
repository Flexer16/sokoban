---
-- ui_data_event.lua


local class = require "middleclass"


local UiDataEvent = class("UiDataEvent")

function UiDataEvent:initialize(data)
    self.event_type = data.event_type
    self.extra = data.extra or nil
end

function UiDataEvent:get_event_type()
    return self.event_type
end

function UiDataEvent:get_extra()
    return self.extra
end

return UiDataEvent
