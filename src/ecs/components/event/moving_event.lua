---
-- moving_event.lua


local class = require "middleclass"


local MovingEvent = class("MovingEvent")

function MovingEvent:initialize(data)
    self.action = data.action
    self.extra = data.extra
    self.player = data.player
end

function MovingEvent:get_action()
    return self.action
end

function MovingEvent:is_action(action)
    if self.action == action then
        return true
    else
        return false
    end
end

function MovingEvent:get_extra_data()
    return self.extra
end

function MovingEvent:is_player()
    return self.player
end

return MovingEvent
