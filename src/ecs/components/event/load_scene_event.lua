---
-- load_scene_event.lua


local class = require "middleclass"


local LoadSceneEvent = class("LoadSceneEvent")

function LoadSceneEvent:initialize(data)
    self.scene_data_path = data.scene_data_path
end

function LoadSceneEvent:get_scene_data_path()
    return self.scene_data_path
end

return LoadSceneEvent
