---
-- beautifire_map_event.lua


local class = require "middleclass"


local BeautifireMapEvent = class("BeautifireMapEvent")

function BeautifireMapEvent:initialize(data)
    self.wall_list = data.wall_list
end

function BeautifireMapEvent:get_wall_list()
    return self.wall_list
end

return BeautifireMapEvent
