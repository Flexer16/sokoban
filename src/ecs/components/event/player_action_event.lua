---
-- player_action_event.lua


local class = require "middleclass"


local PlayerActionEvent = class("PlayerActionEvent")

function PlayerActionEvent:initialize(data)
    self.action = data.action
end

function PlayerActionEvent:get()
    return self.action
end

return PlayerActionEvent
