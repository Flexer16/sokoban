---
-- box_data.lua


local class = require "middleclass"


local BoxData = class("BoxData")

function BoxData:initialize(data)
    self.box = data.box or false
    self.box_point = data.box_point or false

    self.in_position = false
end

function BoxData:is_box()
    return self.box
end

function BoxData:is_box_point()
    return self.box_point
end

function BoxData:is_in_position()
    return self.in_position
end

function BoxData:set_in_position(in_position)
    self.in_position = in_position
end

return BoxData
