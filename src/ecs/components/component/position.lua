---
-- position.lua


local class = require "middleclass"
local vector = require "hump.vector"


local Position = class("Position")

function Position:initialize(data)
    self.pos = vector(0, 0)

    if data then
        self.pos.x = data.x
        self.pos.y = data.y
    end
end

function Position:get()
    return self.pos.x, self.pos.y
end

function Position:set(x, y)
    self.pos.x = x
    self.pos.y = y
end

return Position
