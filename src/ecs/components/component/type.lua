---
-- type.lua


local class = require "middleclass"


local Type = class("Type")

function Type:initialize(data)
    self.type = data.type
end

function Type:get()
    return self.type
end

function Type:is(type)
    if self.type == type then
        return true
    else
        return false
    end
end

return Type
