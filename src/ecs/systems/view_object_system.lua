---
-- view_object_system.lua


local class = require "middleclass"

local Logger = require "utils.log"
local BaseViewSystem = require "ecs.systems.base_view_system"


local ViewObjectSystem = class("ViewObjectSystem", BaseViewSystem)

function ViewObjectSystem:initialize(data)
    BaseViewSystem.initialize(self)

    self.logger = Logger({component = "ViewObjectSystem"})
    self.logger:log("Start!")

    self:_set_tileset("res/tiles/tileset02.png")

    self.tiles = {}

    self:_set_tiles()
end

function ViewObjectSystem:requires()
    return {"Creature"}
end

function ViewObjectSystem:onAddEntity(entity)
    -- body
end

function ViewObjectSystem:onRemoveEntity(entity)
    -- body
end

function ViewObjectSystem:_set_tiles()
    self:_add_tile("@", 1, 10)
    self:_add_tile("b", 1, 5)
end

return ViewObjectSystem
