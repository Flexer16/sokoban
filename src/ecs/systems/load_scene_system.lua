---
-- load_scene_system.lua


local class = require "middleclass"

local Logger = require "utils.log"
local SceneData = require "utils.helpers.scene_data"
local EntityFactory = require "utils.factories.entity_factory"
local EventFactory = require "utils.factories.event_factory"
local EntityTypes = require "utils.enums.entity_types"


local LoadSceneSystem = class("LoadSceneSystem", System)

function LoadSceneSystem:initialize(data)
    System.initialize(self)

    self.logger = Logger({component = "LoadSceneSystem"})
    self.logger:log("Start!")

    self.entity_factory = EntityFactory()
end

function LoadSceneSystem:update(dt)
    for key, entity in pairs(self.targets) do
        self.logger:log("Загрузка новой сцены")

        local event_factory = EventFactory()

        local file_path = entity:get("LoadSceneEvent"):get_scene_data_path()

        self.logger:log("Файл " .. file_path)

        local wall_list = {}

        local scene_data = SceneData({file_path = file_path})
        local map_string = scene_data:get_map_string()

        local x, y = 1, 1

        for row in map_string:gmatch("[^\n]+") do
            x = 1

            for tile in row:gmatch(".") do
                if tile ~= " " then
                    local map_tile = tile

                    if tile == "@" then
                        map_tile = "."

                        local player_entity = self.entity_factory:get_creature_entity({x = x, y = y, tile = tile})
                        engine:addEntity(player_entity)

                        event_factory:rise_add_physics_object(player_entity, true)
                    end

                    if tile == "b" then
                        map_tile = "."

                        local box_entity = self.entity_factory:get_creature_entity({x = x, y = y, tile = tile})
                        engine:addEntity(box_entity)

                        event_factory:rise_add_physics_object(box_entity, false)
                    end

                    local map_entity = self.entity_factory:get_map_entity({x = x, y = y, tile = map_tile})
                    engine:addEntity(map_entity)

                    if map_entity:get("Type"):is(EntityTypes.wall) then
                        table.insert(wall_list, map_entity)
                        event_factory:rise_add_physics_object(map_entity)
                    end
                end

                x = x + 1
            end

            y = y + 1
        end

        event_factory:rise_map_beautifire(wall_list)

        engine:removeEntity(entity)
    end
end

function LoadSceneSystem:requires()
    return {"LoadSceneEvent"}
end

function LoadSceneSystem:onAddEntity(entity)
    -- body
end

function LoadSceneSystem:onRemoveEntity(entity)
    -- body
end

return LoadSceneSystem
