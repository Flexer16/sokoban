---
-- moving_system.lua


local class = require "middleclass"
local bump = require "bump.bump"

local Logger = require "utils.log"
local EntityTypes = require "utils.enums.entity_types"
local MovingActions = require "utils.enums.moving_actions"
local MovingDirections = require "utils.enums.moving_directions"
local EventFactory = require "utils.factories.event_factory"


local MovingSystem = class("MovingSystem", System)

function MovingSystem:initialize(data)
    System.initialize(self)

    self.logger = Logger({component = "MovingSystem"})
    self.logger:log("Start!")

    self.cell_size = 32
    self.world = bump.newWorld(self.cell_size)

    self.player = nil
end

function MovingSystem:update(dt)
    for key, entity in pairs(self.targets) do
        if entity:get("MovingEvent"):is_action(MovingActions.add_entity) then
            local world_item = entity:get("MovingEvent"):get_extra_data()
            local x, y = world_item:get("Position"):get()
            local world_x = self:_get_world_pos(x)
            local world_y = self:_get_world_pos(y)

            self.world:add(world_item, world_x, world_y, self.cell_size, self.cell_size)

            if entity:get("MovingEvent"):is_player() then
                self.player = world_item
            end

            engine:removeEntity(entity)
        end

        if entity:get("MovingEvent"):is_action(MovingActions.remove_all) then
            self.player = nil
            
            local items = self.world:getItems()

            for _, item in ipairs(items) do
                self.world:remove(item)
            end

            local event_factory = EventFactory()
            event_factory:rise_clear_all()

            engine:removeEntity(entity)
        end

        if entity:get("MovingEvent"):is_action(MovingActions.player_move) then
            local direction = entity:get("MovingEvent"):get_extra_data()

            local cur_x, cur_y = self.player:get("Position"):get()
            local world_cur_x, world_cur_y = self:_get_world_pos(cur_x), self:_get_world_pos(cur_y)

            local target_x, target_y = nil, nil

            if direction == MovingDirections.move_up then
                target_x, target_y = self:_get_world_pos(cur_x), self:_get_world_pos(cur_y - 1)
            end

            if direction == MovingDirections.move_down then
                target_x, target_y = self:_get_world_pos(cur_x), self:_get_world_pos(cur_y + 1)
            end

            if direction == MovingDirections.move_left then
                target_x, target_y = self:_get_world_pos(cur_x - 1), self:_get_world_pos(cur_y)
            end

            if direction == MovingDirections.move_right then
                target_x, target_y = self:_get_world_pos(cur_x + 1), self:_get_world_pos(cur_y)
            end

            local actual_x, actual_y, cols = self.world:move(self.player, target_x, target_y)

            for _, collision in ipairs(cols) do
                if collision.other:get("Type"):is(EntityTypes.box) then
                    local box_position = collision.other:get("Position")

                    local box_target_x, box_target_y = self:_normal_to_box_direction(collision.normal, box_position)

                    local box_actual_x, box_actual_y = self.world:move(collision.other, box_target_x, box_target_y)
                    collision.other:get("Position"):set(self:_get_local_pos(box_actual_x), self:_get_local_pos(box_actual_y))

                    actual_x, actual_y = self.world:move(self.player, target_x, target_y)
                end
            end

            self.player:get("Position"):set(self:_get_local_pos(actual_x), self:_get_local_pos(actual_y))

            engine:removeEntity(entity)
        end
    end
end

function MovingSystem:requires()
    return {"MovingEvent"}
end

function MovingSystem:onAddEntity(entity)
    -- body
end

function MovingSystem:onRemoveEntity(entity)
    -- body
end

function MovingSystem:_get_world_pos(local_pos)
    return (local_pos - 1) * self.cell_size
end

function MovingSystem:_get_local_pos(world_pos)
    return world_pos / self.cell_size + 1
end

function MovingSystem:_normal_to_box_direction(normal, box_position)
    assert(box_position)
    local cur_x, cur_y = box_position:get()

    if normal.x == 1 and normal.y == 0 then
        return self:_get_world_pos(cur_x - 1), self:_get_world_pos(cur_y)
    end

    if normal.x ==  -1 and normal.y == 0 then
        return self:_get_world_pos(cur_x + 1), self:_get_world_pos(cur_y)
    end

    if normal.x == 0 and normal.y == 1 then
        return self:_get_world_pos(cur_x), self:_get_world_pos(cur_y - 1)
    end

    if normal.x == 0 and normal.y ==  -1 then
        return self:_get_world_pos(cur_x), self:_get_world_pos(cur_y + 1)
    end
end

return MovingSystem
