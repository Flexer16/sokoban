---
-- clear_scene_system.lua


local class = require "middleclass"

local Logger = require "utils.log"
local EventFactory = require "utils.factories.event_factory"


local ClearSceneSystem = class("ClearSceneSystem", System)

function ClearSceneSystem:initialize(data)
    System.initialize(self)

    self.logger = Logger({component = "ClearSceneSystem"})
    self.logger:log("Start!")
end

function ClearSceneSystem:update(dt)
    for key, entity in pairs(self.targets) do
        self.logger:log("Очистка игровой сцены")

        local entities = engine:getEntitiesWithComponent("Position")

        for _, entity in pairs(entities) do
            engine:removeEntity(entity)
        end

        local event_factory = EventFactory()
        event_factory:rise_load_scene("res/data/map.json")

        engine:removeEntity(entity)
    end
end

function ClearSceneSystem:requires()
    return {"ClearEvent"}
end

function ClearSceneSystem:onAddEntity(entity)
    -- body
end

function ClearSceneSystem:onRemoveEntity(entity)
    -- body
end

return ClearSceneSystem
