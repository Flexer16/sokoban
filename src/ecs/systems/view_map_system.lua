---
-- view_map_system.lua


local class = require "middleclass"

local Logger = require "utils.log"
local BaseViewSystem = require "ecs.systems.base_view_system"


local ViewMapSystem = class("ViewMapSystem", BaseViewSystem)

function ViewMapSystem:initialize(data)
    BaseViewSystem.initialize(self)

    self.logger = Logger({component = "ViewMapSystem"})
    self.logger:log("Start!")

    self:_set_tileset("res/tiles/tileset02.png")

    self.tiles = {}

    self:_set_tiles()
end

function ViewMapSystem:requires()
    return {"Map"}
end

function ViewMapSystem:onAddEntity(entity)
    -- body
end

function ViewMapSystem:onRemoveEntity(entity)
    -- body
end

function ViewMapSystem:_set_tiles()
    -- wall
    self:_add_tile(0, 1, 1)
    self:_add_tile(1, 2, 1)
    self:_add_tile(2, 3, 1)
    self:_add_tile(3, 4, 1)
    self:_add_tile(4, 5, 1)
    self:_add_tile(5, 6, 1)
    self:_add_tile(6, 7, 1)
    self:_add_tile(7, 8, 1)
    self:_add_tile(8, 9, 1)
    self:_add_tile(9, 10, 1)
    self:_add_tile(10, 1, 2)
    self:_add_tile(11, 2, 2)
    self:_add_tile(12, 3, 2)
    self:_add_tile(13, 4, 2)
    self:_add_tile(14, 5, 2)
    self:_add_tile(15, 6, 2)

    -- floor
    self:_add_tile(".", 2, 3)
    self:_add_tile("X", 7, 3)

    -- wall
    self:_add_tile("#", 1, 1)
end


return ViewMapSystem
