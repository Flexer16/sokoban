---
-- beautifier_map_system.lua


local class = require "middleclass"

local Logger = require "utils.log"


local BeautifierMapSystem = class("BeautifierMapSystem", System)

function BeautifierMapSystem:initialize(data)
    System.initialize(self)

    self.logger = Logger({component = "BeautifierMapSystem"})
    self.logger:log("Start!")
end

function BeautifierMapSystem:update(dt)
    for key, entity in pairs(self.targets) do
        self.logger:log("Обработка карты")

        local wall_list = entity:get("BeautifireMapEvent"):get_wall_list()

        for _, wall in ipairs(wall_list) do
            local neigh_data = 0
            
            local cur_x, cur_y = wall:get("Position"):get()

            for _, finger_wall in ipairs(wall_list) do
                local finder_x, finder_y = finger_wall:get("Position"):get()

                if finder_x == cur_x and finder_y == cur_y - 1 then
                    neigh_data = neigh_data + 1
                end

                if finder_x == cur_x and finder_y == cur_y + 1 then
                    neigh_data = neigh_data + 8
                end

                if finder_x == cur_x - 1 and finder_y == cur_y then
                    neigh_data = neigh_data + 2
                end

                if finder_x == cur_x + 1 and finder_y == cur_y then
                    neigh_data = neigh_data + 4
                end
            end

            wall:get("Tile"):set(neigh_data)
        end

        engine:removeEntity(entity)
    end
end

function BeautifierMapSystem:requires()
    return {"BeautifireMapEvent"}
end

function BeautifierMapSystem:onAddEntity(entity)
    -- body
end

function BeautifierMapSystem:onRemoveEntity(entity)
    -- body
end

return BeautifierMapSystem
