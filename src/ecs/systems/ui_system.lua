---
-- ui_system.lua


local class = require "middleclass"
local suit = require "suit"

local Logger = require "utils.log"
local EventFactory = require "utils.factories.event_factory"


local UiSystem = class("UiSystem", System)

function UiSystem:initialize(data)
    System.initialize(self)

    self.logger = Logger({component = "UiSystem"})
    self.logger:log("Start!")

    self.ui = suit.new()

    self.button_widht = 300
    self.button_height = 32

    self.boxes = 0
    self.box_points = 0

    self.game_state = true
end

function UiSystem:update(dt)
    for key, entity in pairs(self.targets) do
        if entity:get("UiDataEvent"):get_event_type() == "new_box_count" then
            local extra = entity:get("UiDataEvent"):get_extra()

            self.boxes = extra.box
            self.box_points = extra.box_point
        end

        if entity:get("UiDataEvent"):get_event_type() == "win" then
            if self.game_state then
                self.game_state = false
            end
        end

        engine:removeEntity(entity)
    end

    if self.game_state then
        self.ui:Label("BOX: " .. self.boxes .. "/".. self.box_points .. "", (love.graphics.getWidth() / 2) - self.button_widht, love.graphics.getHeight() - (self.button_height * 5), self.button_widht, self.button_height)

        if self.ui:Button("Restart", 0, love.graphics.getHeight() - self.button_height, self.button_widht, self.button_height).hit then
            local event_factory = EventFactory()
            event_factory:rise_remove_all_objects()
        end

        if self.ui:Button("Exit", love.graphics.getWidth() - self.button_widht, love.graphics.getHeight() - self.button_height, self.button_widht, self.button_height).hit then
            self.logger:log("Exit game!")
            love.event.quit()
        end
    else
        self.ui:Label("You win!!!", (love.graphics.getWidth() / 2) - self.button_widht, love.graphics.getHeight() - (self.button_height * 5), self.button_widht, self.button_height)

        if self.ui:Button("Restart", (love.graphics.getWidth() / 2) - self.button_widht, love.graphics.getHeight() - (self.button_height * 4), self.button_widht, self.button_height).hit then
            if not self.game_state then
                self.game_state = true
            end

            local event_factory = EventFactory()
            event_factory:rise_remove_all_objects()
        end
    end
end

function UiSystem:draw()
    self.ui:draw()
end

function UiSystem:requires()
    return {"UiDataEvent"}
end

function UiSystem:onAddEntity(entity)
    -- body
end

function UiSystem:onRemoveEntity(entity)
    -- body
end

return UiSystem
