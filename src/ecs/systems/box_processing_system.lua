---
-- box_processing_system.lua


local class = require "middleclass"

local Logger = require "utils.log"
local EventFactory = require "utils.factories.event_factory"


local BoxProcessingSystem = class("BoxProcessingSystem", System)

function BoxProcessingSystem:initialize(data)
    System.initialize(self)

    self.logger = Logger({component = "BoxProcessingSystem"})
    self.logger:log("Start!")

    self.box_list = {}
    self.box_point_list = {}

    self.previouse_point_count = nil
    self.previouse_in_position = nil
end

function BoxProcessingSystem:update(dt)
    for _, box in ipairs(self.box_list) do
        local cur_x, cur_y = box:get("Position"):get()

        local stand = false

        for _, box_positions in ipairs(self.box_point_list) do
            local cur_pos_x, cur_pos_y = box_positions:get("Position"):get()

            if cur_x == cur_pos_x and cur_y == cur_pos_y then
                stand = true
                box:get("BoxData"):set_in_position(true)
            end
        end

        if not stand then
            box:get("BoxData"):set_in_position(false)
        end
    end

    local in_position = 0

    for _, box in ipairs(self.box_list) do
        if box:get("BoxData"):is_in_position() then
            in_position = in_position + 1
        end
    end

    if self.previouse_in_position ~= in_position then
        self.previouse_in_position = in_position

        local event_factory = EventFactory()
        event_factory:rise_ui_data("new_box_count", {box = self.previouse_in_position, box_point = #self.box_point_list})
    end

    if #self.box_point_list ~= self.previouse_point_count then
        local event_factory = EventFactory()
        event_factory:rise_ui_data("new_box_count", {box = self.previouse_in_position, box_point = #self.box_point_list})

        self.previouse_point_count = #self.box_point_list
    end

    if self.previouse_in_position == #self.box_point_list and self.previouse_in_position > 0 and #self.box_point_list > 0 then
        local event_factory = EventFactory()
        event_factory:rise_ui_data("win")
        self.previouse_in_position = nil
    end
end

function BoxProcessingSystem:requires()
    return {"BoxData"}
end

function BoxProcessingSystem:onAddEntity(entity)
    if entity:get("BoxData"):is_box() then
        table.insert(self.box_list, entity)
    end

    if entity:get("BoxData"):is_box_point() then
        table.insert(self.box_point_list, entity)
    end
end

function BoxProcessingSystem:onRemoveEntity(entity)
    if entity:get("BoxData"):is_box() then
        for i, box in ipairs(self.box_list) do
            if box == entity then
                table.remove(self.box_list, i)
                break
            end
        end
    end

    if entity:get("BoxData"):is_box_point() then
        for i, box_point in ipairs(self.box_point_list) do
            if box_point == entity then
                table.remove(self.box_point_list, i)
                break
            end
        end
    end
end

return BoxProcessingSystem
