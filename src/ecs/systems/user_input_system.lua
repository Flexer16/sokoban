---
-- user_input_system.lua


local class = require "middleclass"
local Input = require "input"

local Logger = require "utils.log"
local EventFactory = require "utils.factories.event_factory"
local MovingDirections = require "utils.enums.moving_directions"


local UserInputSystem = class("UserInputSystem", System)

function UserInputSystem:initialize(data)
    System.initialize(self)

    self.logger = Logger({component = "UserInputSystem"})
    self.logger:log("Start!")

    self.input = Input()

    self.key_table = {
        {"up", MovingDirections.move_up},
        {"down", MovingDirections.move_down},
        {"left", MovingDirections.move_left},
        {"right", MovingDirections.move_right}
    }

    self:_set_key_table()
end

function UserInputSystem:update(dt)
    for _, val in ipairs(self.key_table) do
        if self.input:down(val[2], 0.10) then
            self.logger:log("Нажата кнопка " .. val[1])

            local event_factory = EventFactory()
            event_factory:rise_player_action(val[2])
        end
    end
end

function UserInputSystem:requires()
    return {}
end

function UserInputSystem:onAddEntity(entity)
    -- body
end

function UserInputSystem:onRemoveEntity(entity)
    -- body
end

function UserInputSystem:_set_key_table()
    for _, val in ipairs(self.key_table) do
        self.input:bind(val[1], val[2])
    end
end

return UserInputSystem
