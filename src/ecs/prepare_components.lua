---
-- prepare_components.lua


Component.register(require "ecs.components.event.load_scene_event")
Component.register(require "ecs.components.event.beautifire_map_event")
Component.register(require "ecs.components.event.player_action_event")
Component.register(require "ecs.components.event.moving_event")
Component.register(require "ecs.components.event.clear_event")
Component.register(require "ecs.components.event.ui_data_event")

Component.register(require "ecs.components.component.position")
Component.register(require "ecs.components.component.tile")
Component.register(require "ecs.components.component.type")
Component.register(require "ecs.components.component.map")
Component.register(require "ecs.components.component.creature")
Component.register(require "ecs.components.component.player")
Component.register(require "ecs.components.component.box_data")
