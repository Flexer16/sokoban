---
-- main.lua


package.path = package.path .. ";lib/?/init.lua;lib/?.lua;src/?.lua"


local lovetoys = require "lovetoys"

local Logger = require "utils.log"
local EventFactory = require "utils.factories.event_factory"


lovetoys.initialize({
    globals = true,
    debug = true
})


require "ecs.prepare_components"


local LoadSceneSystem = require "ecs.systems.load_scene_system"
local ViewMapSystem = require "ecs.systems.view_map_system"
local ViewObjectSystem = require "ecs.systems.view_object_system"
local BeautifierMapSystem = require "ecs.systems.beautifier_map_system"
local UserInputSystem = require "ecs.systems.user_input_system"
local MovingSystem = require "ecs.systems.moving_system"
local UiSystem = require "ecs.systems.ui_system"
local ClearSceneSystem = require "ecs.systems.clear_scene_system"
local BoxProcessingSystem = require "ecs.systems.box_processing_system"


local logger = {}


function love.load()
    logger = Logger()

    engine = Engine()

    local ui_system = UiSystem()

    engine:addSystem(UserInputSystem(), "update")

    engine:addSystem(LoadSceneSystem(), "update")
    engine:addSystem(BeautifierMapSystem(), "update")
    engine:addSystem(MovingSystem(), "update")
    engine:addSystem(ui_system, "update")
    engine:addSystem(ClearSceneSystem(), "update")
    engine:addSystem(BoxProcessingSystem(), "update")

    engine:addSystem(ViewMapSystem(), "draw")
    engine:addSystem(ViewObjectSystem(), "draw")
    engine:addSystem(ui_system, "draw")

    local event_factory = EventFactory()
    event_factory:rise_load_scene("res/data/map.json")
end


function love.update(dt)
    if dt < 1/60 then
        love.timer.sleep(1/60 - dt)
    end

    engine:update(dt)

    logger:update()
end


function love.draw()
    engine:draw()
end
